﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BookService.WebAPI.Models
{
     public class BookServiceDbContext : IdentityDbContext<ApplicationUser>
    {
        public BookServiceDbContext()
            : base("BookServiceDbConnection", throwIfV1Schema: false)
        {
        }

        public static BookServiceDbContext Create()
        {
            return new BookServiceDbContext();
        }

        public DbSet<Author> Authors { get; set; }

        public DbSet<Book> Books { get; set; }
    }
}

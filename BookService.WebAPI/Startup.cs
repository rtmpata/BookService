﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BookService.WebAPI.Startup))]
namespace BookService.WebAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
